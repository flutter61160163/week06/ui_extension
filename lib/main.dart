import 'package:flutter/material.dart';
import 'package:ui_extension/comboboxtile_widgit.dart';
import 'package:ui_extension/checkbox_widget.dart';
import 'package:ui_extension/dropdown_winget.dart';
import 'package:ui_extension/radio_widget.dart';

void main() {
  runApp(MaterialApp(title: 'UI Extension', home: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('UI Extension')),
      drawer: Drawer(
        child: ListView(
          children: [
            GestureDetector(
              child: DrawerHeader(
                child: Text('UI Menu'),
                decoration: BoxDecoration(color: Colors.blue),
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('CheckBox'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => CheckBoxWidget()));
              },
            ),
            ListTile(
              title: Text('ComboBoxTile'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ComboBoxTileWidget()));
              },
            ),
            ListTile(
              title: Text('Dropdown'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => DropDownWidget()));
              },
            ),
            ListTile(
              title: Text('Radio'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => RadioWidget()));
              },
            ),
          ],
        ),
      ),
      body: ListView(
        children: [
          ListTile(
            title: Text('CheckBox'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => CheckBoxWidget()));
            },
          ),
          ListTile(
            title: Text('ComboBoxTile'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ComboBoxTileWidget()));
            },
          ),
          ListTile(
            title: Text('Dropdown'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => DropDownWidget()));
            },
          ),
          ListTile(
            title: Text('Radio'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => RadioWidget()));
            },
          ),
        ],
      ),
    );
  }
}
